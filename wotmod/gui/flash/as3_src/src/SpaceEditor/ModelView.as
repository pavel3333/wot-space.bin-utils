package SpaceEditor
{
	import SpaceEditor.components.TransformInput;
	import net.wg.gui.components.controls.*;

	public class ModelView extends BaseView
	{
		private var _casts_shadow: CheckBox;
		private var _casts_local_shadow: CheckBox;
		private var _reflection_visible: CheckBox;
		private var _resource: TextInput;
		private var _transform: TransformInput;

		public function ModelView()
		{
			super();

			_casts_shadow = getComponent("CheckBox", CheckBox , {
				label: "castsShadow",
				y: 10
			});

			_casts_local_shadow = getComponent("CheckBox", CheckBox , {
				label: "castsLocalShadow",
				y: 40
			});

			_reflection_visible = getComponent("CheckBox", CheckBox , {
				label: "reflectionVisible",
				y: 70
			});

			_resource = getTextInput("resource:", 100);

			_transform = getTransformInput("transform:", 160);
		}

		override public function setData(data: Object) : void
		{
			_casts_shadow.selected = data.casts_shadow;
			_casts_local_shadow.selected = data.casts_local_shadow;
			_reflection_visible.selected = data.reflection_visible;
			_resource.text = data.resource;
			_transform.value = data.transform;
		}

		override public function getData() : Object
		{
			var data: Object = new Object();
			data.casts_shadow = _casts_shadow.selected;
			data.casts_local_shadow = _casts_local_shadow.selected;
			data.reflection_visible = _reflection_visible.selected;
			data.resource = _resource.text;
			data.transform = _transform.value;
			return data;
		}
	}
}
