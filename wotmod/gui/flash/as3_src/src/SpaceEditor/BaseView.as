package SpaceEditor 
{
	import flash.display.*;
	import SpaceEditor.components.*;
	import net.wg.gui.components.controls.*;

	public class BaseView extends BaseUtils
	{
		public var scrollPane: ScrollPane;

		public function BaseView() 
		{
			super();

			scrollPane = new ScrollPane();
			scrollPane.scrollBar = "ScrollBar";
			scrollPane.scrollStepFactor = 15.0;
			scrollPane.target = this;
		}

		public function clear() : void
		{
			scrollPane.removeChild(this);
			scrollPane.target = null;
			scrollPane.parent.removeChild(scrollPane);
			scrollPane = null;
		}

		public function setData(data: Object) : void
		{
			// nothing here =)
		}

		public function getData() : Object
		{
			return null;
		}
	}
}
