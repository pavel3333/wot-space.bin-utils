package SpaceEditor
{
	import net.wg.gui.components.controls.*;

	public class ParticlesView extends BaseView
	{
		private var reflection_visible: CheckBox;

		public function ParticlesView()
		{
			super();

			reflection_visible = getComponent("CheckBox", CheckBox , {
				label: "reflectionVisible",
				y: 10
			});
		}

		override public function setData(data: Object) : void
		{
			reflection_visible.selected = data.reflection_visible;
		}

		override public function getData() : Object
		{
			var data: Object = new Object();
			data.reflection_visible = reflection_visible.selected;
			return data;
		}
	}
}
