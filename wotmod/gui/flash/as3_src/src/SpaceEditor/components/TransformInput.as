package SpaceEditor.components 
{
	import flash.display.Sprite;
	import net.wg.gui.components.controls.LabelControl;

	public class TransformInput extends BaseUtils
	{
		private var row0: Vector3Input;
		private var row1: Vector3Input;
		private var row2: Vector3Input;
		private var row3: Vector3Input;

		public function TransformInput() 
		{
			super();
			row0 = getVector3Input("row0", 0, false);
			row1 = getVector3Input("row1", 25, false);
			row2 = getVector3Input("row2", 50, false);
			row3 = getVector3Input("row3", 75, false);
		}

		public function set value(n:Array):void
		{
			row0.value = n[0];
			row1.value = n[1];
			row2.value = n[2];
			row3.value = n[3];
		}

		public function get value():Array
		{
			return [
				row0.value,
				row1.value,
				row2.value,
				row3.value
			];
		}
	}
}
