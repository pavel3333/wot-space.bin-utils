﻿''' ShadowHunterRUS - 2018 '''

print('mod_freecam: https://koreanrandom.com/forum/topic/29351-/')



import ResMgr
import AvatarInputHandler
import BigWorld

from gui import g_mouseEventHandlers, InputHandler
from gui.shared.utils.HangarSpace import HangarVideoCameraController
from AvatarInputHandler.VideoCamera import VideoCamera



def camera_init(self):
	rootSection = ResMgr.openSection(AvatarInputHandler._INPUT_HANDLER_CFG)
	if rootSection is None:
		return
	videoSection = rootSection['videoMode']
	if videoSection is None:
		return
	videoCameraSection = videoSection['camera']
	self._HangarVideoCameraController__videoCamera = VideoCamera(videoCameraSection)
	self._HangarVideoCameraController__overriddenCamera = BigWorld.camera()
	InputHandler.g_instance.onKeyDown += self.handleKeyEvent
	InputHandler.g_instance.onKeyUp += self.handleKeyEvent
	g_mouseEventHandlers.add(self.handleMouseEvent)



orig_camera_destroy = HangarVideoCameraController.destroy
def camera_destroy(self):
	self._HangarVideoCameraController__enabled = False
	orig_camera_destroy(self)



HangarVideoCameraController.init = camera_init
HangarVideoCameraController.destroy = camera_destroy
