﻿''' ShadowHunterRUS - 2018 '''

print('mod_wot_hangar_utils: https://koreanrandom.com/forum/topic/29351-/')



import ResMgr

from gui import InputHandler
from gui.shared.utils.key_mapping import getBigworldNameFromKey
from gui.shared.utils.HangarSpace import HangarSpace, IHangarSpace
from constants import IS_CHINA
import gui.ClientHangarSpace as chs
from gui.app_loader.loader import g_appLoader
from gui.Scaleform.framework import ViewTypes
from helpers import dependency

chs._SERVER_CMD_CHANGE_HANGAR = ''
chs._SERVER_CMD_CHANGE_HANGAR_PREM = ''



class ModHangarUtils:
	hangarSpace = dependency.descriptor(IHangarSpace)
	blocked = False

	generate_space_bin = None
	offline_hangar = None

	space_path = None
	hangar_name = None
	models = None
	c_space = None
	__strings = None

	@classmethod
	def set_block(cls, value):
		cls.blocked = value

	def __init__(self):
		if self.blocked:
			print('ModHangarUtils is blocked')
			return
		self.set_block(True)
		self.space_path = ResMgr.resolveToAbsolutePath('spaces/%s' % self.hangar_name)
		try:
			if self.generate_space_bin:
				self.create_space_bin()
			chs._getDefaultHangarPath = lambda *args: 'spaces/%s' % self.hangar_name
			#chs.readHangarSettings('igrPremHangarPath' + ('CN' if IS_CHINA else ''))
		except:
			import traceback
			traceback.print_exc()
		self.set_block(False)

	def sections_from_hangar_chunk(self):
		chunk = ResMgr.openSection('spaces/%s/hangar.chunk' % self.hangar_name)
		if chunk is None:
			return
		lights = []
		models = []
		particles = []
		speedtrees = []
		list_audio = []

		if self.offline_hangar:
			spacebin_editor.SpaceEditorWindow.clear()
			spacebin_editor.SpaceEditorWindow.chunk = chunk

		for id, (tag, value) in enumerate(chunk.items()):
			if self.offline_hangar:
				spacebin_editor.SpaceEditorWindow.addData(id, tag, value)
			if tag in ('omniLight', 'spotLight', 'pulseLight', 'pulseSpotLight'):
				lights.append((tag, value))
			elif tag == 'model':
				models.append(value)
			elif tag == 'particles':
				particles.append(value)
			elif tag == 'speedtree':
				speedtrees.append(value)
			elif tag == 'audio':
				list_audio.append(value)

		bwst = self.c_space.sections['BWST']
		bsmo = self.c_space.sections['BSMO']
		bsma = self.c_space.sections['BSMA']
		bwsg = self.c_space.sections['BWSG']
		bsgd = self.c_space.sections['BSGD']
		bwal = self.c_space.sections['BWAL']
		self.c_space.sections['BSMI'].save_models(bwst, bwal, bsmo, bsma, bwsg, bsgd, models)
		self.c_space.sections['BWLC'].from_chunk(bwst, lights)
		self.c_space.sections['BWPs'].from_chunk(bwst, bwal, particles)
		self.c_space.sections['SpTr'].from_chunk(bwst, speedtrees)
		self.c_space.sections['WTau'].from_chunk(bwst, list_audio)

	def create_space_bin(self):
		self.set_block(True)
		from compiled_space import CompiledSpace
		self.c_space = CompiledSpace()
		self.c_space.from_dir('%s/space_bin_kitchen' % self.space_path)
		self.sections_from_hangar_chunk()
		self.c_space.save_to_bin('%s/space.bin' % self.space_path)
		self.set_block(False)


hide_interface = True


def main():
	current_xml = ResMgr.openSection('spaces/current.xml')
	if current_xml is None:return

	ModHangarUtils.hangar_name = current_xml.readString('space', '')
	if not ModHangarUtils.hangar_name:return
	ModHangarUtils.generate_space_bin = current_xml.readBool('generate_space_bin', True)
	ModHangarUtils.offline_hangar = current_xml.readBool('offline_hangar', False)
	if ModHangarUtils.offline_hangar:
		try:
			global spacebin_editor
			import spacebin_editor
			spacebin_editor.init(ModHangarUtils)
			ModHangarUtils.generate_space_bin = True
		except:
			import traceback
			traceback.print_exc()
			ModHangarUtils.offline_hangar = False

	ModHangarUtils()

	refresh_space_hotkey = current_xml.readString('refresh_space_hotkey', 'KEY_F10')

	if not ModHangarUtils.offline_hangar:
		hide_interface_hotkey = current_xml.readString('hide_interface_hotkey', 'KEY_F11')
	else:
		hide_interface_hotkey = None

	def onhandleKeyEvent(event):
		key = getBigworldNameFromKey(event.key)
		if key == refresh_space_hotkey and not ModHangarUtils.blocked:
			try:
				isPremium = ModHangarUtils.hangarSpace.isPremium
				ModHangarUtils.hangarSpace.destroy()
				ModHangarUtils()
			except:
				import traceback
				traceback.print_exc()
			finally:
				pass
				ModHangarUtils.hangarSpace.init(isPremium)
		elif hide_interface_hotkey and key == hide_interface_hotkey:
			global hide_interface
			hide_interface ^= 1
			try:
				app = g_appLoader.getApp()
				app.component.visible = hide_interface
				app.graphicsOptimizationManager.switchOptimizationEnabled(hide_interface)
			except:
				import traceback
				traceback.print_exc()

	orig_h_init = HangarSpace.init
	orig_h_destroy = HangarSpace.destroy

	def h_init(*args):
		base = orig_h_init(*args)
		InputHandler.g_instance.onKeyDown += onhandleKeyEvent
		return base

	HangarSpace.init = h_init

	def h_destroy(*args):
		base = orig_h_destroy(*args)
		InputHandler.g_instance.onKeyDown -= onhandleKeyEvent
		return base

	HangarSpace.destroy = h_destroy



def init():
	main()
