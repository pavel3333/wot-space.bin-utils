import sys
from PyQt5.QtWidgets import QApplication, QWidget, QListWidget, QTreeWidget, QTreeWidgetItem
from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QHBoxLayout

from compiled_space import CompiledSpace



space_bin_unpacked_path = './tests/0.9.21.0.3/spaces/02_malinovka/unpacked/'



class SpaceBinViewerWindow(QWidget):
	current_sec = None

	def __init__(self):
		super().__init__()
		self.cspace = CompiledSpace()
		self.cspace.from_dir(space_bin_unpacked_path)
		self.initUI()

	def showSection(self):
		self.treeWidget.clear()
		sec = self.cspace.sections[self.current_sec]
		if sec.header != 'BWSG':
			strings = self.cspace.sections['BWST']._data
		else:
			strings = sec._data['strings']
		sec.setDataForPyQt(self.treeWidget, strings, QTreeWidgetItem)

	def initData(self):
		for sec in self.cspace.sections:
			self.listWidget.addItem(sec)

	def listClicked(self, item):
		if self.current_sec != item.text():
			self.current_sec = item.text()
			self.showSection()

	def initUI(self):
		self.setMinimumSize(QSize(640, 480))
		self.hLayout = QHBoxLayout(self)
		self.listWidget = QListWidget(self)
		self.listWidget.setMaximumWidth(200)
		self.listWidget.itemClicked.connect(self.listClicked)
		self.treeWidget = QTreeWidget(self)
		self.treeWidget.setColumnCount(2)
		self.treeWidget.setColumnWidth(0, 200)
		self.treeWidget.header().setVisible(False)
		self.hLayout.addWidget(self.listWidget)
		self.hLayout.addWidget(self.treeWidget)
		self.setWindowTitle('space.bin viewer {}'.format(space_bin_unpacked_path))
		self.initData()
		self.show()



app = QApplication(sys.argv)
wnd = SpaceBinViewerWindow()
sys.exit(app.exec_())
