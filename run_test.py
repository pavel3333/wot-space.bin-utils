import os
import logging
logging.basicConfig(filename='testing.log',  filemode='w', level=logging.INFO)

from glob import glob
from compiled_space import CompiledSpace



for vpath in glob('.\\tests\\*'):
	if not os.path.isdir(vpath):
		continue
	ver = os.path.basename(vpath)
	for space_path in glob('.\\tests\\%s\\spaces\\*' % ver):
		try:
			logging.info('\norig: %s' % space_path)
			print('orig: %s' % space_path)
			bin_file_name_in = os.path.join(space_path, 'space.bin')
			with open(bin_file_name_in, 'rb') as fr:
				space1 = CompiledSpace(fr, ver)

			unp_dir = os.path.join(space_path, 'unpacked')
			if not os.path.exists(unp_dir):
				os.mkdir(unp_dir)
			space1.unp_to_dir(unp_dir)

			space1.unp_for_world_editor(space_path)

			pck_dir = os.path.join(space_path, 'packed')
			if not os.path.exists(pck_dir):
				os.mkdir(pck_dir)
			space2 = CompiledSpace()
			space2.from_dir(unp_dir)
			space2.save_to_bin(os.path.join(pck_dir, 'space.bin'))

			logging.info('new: %s' % space_path)
			bin_file_name_in = os.path.join(pck_dir, 'space.bin')
			with open(bin_file_name_in, 'rb') as fr:
				space3 = CompiledSpace(fr, ver)

		except:
			import traceback
			traceback.print_exc()
			break
