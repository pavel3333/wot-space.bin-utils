﻿import os.path as path
from glob import glob
from zipfile import ZipFile



pkg_path_mask = 'E:/Games/World_of_Tanks/res/packages/*.pkg'



for pkg_abs_path in glob(pkg_path_mask):
	name = path.basename(pkg_abs_path)
	name = path.splitext(name)[0]
	try:
		with ZipFile(pkg_abs_path, 'r') as zf:
			zf.extract('spaces/%s/space.bin' % name, './0.9.21')
	except:
		pass
