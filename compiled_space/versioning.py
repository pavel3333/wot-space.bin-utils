﻿""" Versioning - as in res_mods/ """

from sections import (
	all_sections_cls_0_9_12, all_sections_cls_0_9_14,
	all_sections_cls_0_9_16, all_sections_cls_0_9_20,
	all_sections_cls_1_0_0, all_sections_cls_1_0_1,
	all_sections_cls_1_1_0)



WOT_VERSIONS = [
	{
		'sections': all_sections_cls_0_9_12,
		'versions': (
			'0.9.12', # WoT init space.bin
			'0.9.13',
		)
	},
	{
		'sections': all_sections_cls_0_9_14,
		'versions': (
			'0.9.14',
		)
	},
	{
		'sections': all_sections_cls_0_9_16,
		'versions': (
			'0.9.16',
		)
	},
	{
		'sections': all_sections_cls_0_9_20,
		'versions': (
			'0.9.17.0', '0.9.17.1',
			'0.9.20.0', '0.9.20.1.4'
			'0.9.21.0', '0.9.21.0.1', '0.9.21.0.2'
			'0.9.22.0', '0.9.22.0.1'
		)
	},
	{
		'sections': all_sections_cls_1_0_0,
		'versions': (
			'1.0.0', '1.0.0.2', '1.0.0.3',
		)
	},
	{
		'sections': all_sections_cls_1_0_1,
		'versions': (
			'1.0.1.0', '1.0.1.1',
			'1.0.2.0', '1.0.2.1',
		)
	},
	{
		'sections': all_sections_cls_1_1_0,
		'versions': (
			'1.1.0', '1.1.0.1', # actual
		)
	}
]



def get_sections(ver):
	for it in WOT_VERSIONS:
		if ver in it['versions']:
			return it['sections']
	raise Exception('version %s is not supported' % ver)
