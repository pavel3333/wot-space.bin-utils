from collections import OrderedDict
import logging



try:
	import ctypes
	from ctypes import c_float, c_uint8, c_int16, c_uint32, c_int32

	class CStructure(ctypes.LittleEndianStructure):
		def __init__(self, data):
			super(CStructure, self).__init__()
			assert ctypes.sizeof(self) == self._size_, (ctypes.sizeof(self), self)
			if isinstance(data, dict):
				self.from_dict(data)
			else:
				self.from_bin(data)

		def to_bin(self):
			try:
				return buffer(self)[:]
			except:
				return bytes(self)

		def to_dict(self):
			result = OrderedDict()
			for field in self._fields_:
				name = field[0]
				attr = getattr(self, name)
				if hasattr(attr, '_length_'):
					result[name] = tuple(attr)
				else:
					result[name] = attr
			return result

		def from_dict(self, data):
			for key, val in data.items():
				if hasattr(self, key):
					if hasattr(val, '__len__'):
						exec('self.%s = tuple(val)' % (key))
					else:
						exec('self.%s = val' % key)

		def from_bin(self, data):
			ctypes.memmove(ctypes.addressof(self), data, self._size_)
			self.tests()

		def test_failed(self, field, value, expr, expected_value):
			logging.info('\nTest failed')
			logging.info('obj: %s' % self)
			logging.info('field "%s" = %s' % (field, value))
			logging.info('test: %s %s' % (expr, expected_value))

		def tests(self):
			if not hasattr(self, '_tests_'):return
			for field, testsDict in self._tests_.items():
				for expr, expected_value in testsDict.items():
					value = getattr(self, field)
					if hasattr(value, '__len__'):
						for idx, it in enumerate(expected_value):
							exec('if not value[idx] '+ expr +' it:self.test_failed(field+"[%s]", value, expr, it)' % idx)
					else:
						exec('if not value '+ expr +' expected_value:self.test_failed(field, value, expr, expected_value)')

except:
	from struct import unpack, pack, calcsize

	c_float = 'f'
	c_uint8 = 'b'
	c_uint32 = 'I'
	c_int32 = 'i'
	c_int16 = 'h'

	class CStructure:
		def __init__(self, data):
			if not hasattr(self, '__data_fmt'):
				self.prepare_cls()
			self.__data = []
			if isinstance(data, dict):
				self.from_dict(data)
			else:
				self.from_bin(data)

		@classmethod
		def prepare_cls(cls):
			cls.__data_fmt = '<'
			latest_sz = 0
			for field in cls._fields_:
				if len(field) == 2:
					if latest_sz > 0:
						cls.__data_fmt += 'I'
						latest_sz = 0
						assert False
					cls.__data_fmt += field[1]
				else:
					latest_sz += field[2]
					if latest_sz == 32:
						cls.__data_fmt += field[1]
						latest_sz = 0
					elif latest_sz > 32:
						assert False

		def to_bin(self):
			lst = []
			latest_sz, val = 0, 0
			for i, field in enumerate(self._fields_):
				if len(field) == 2:
					if latest_sz > 0:
						lst.append(val)
						latest_sz, val = 0, 0
						assert False
					if hasattr(self.__data[i], '__len__'):
						lst += self.__data[i]
					else:
						lst.append(self.__data[i])
				else:
					val |= self.__data[i] << latest_sz
					latest_sz += field[2]
					if latest_sz == 32:
						lst.append(val)
						latest_sz, val = 0, 0
					elif latest_sz > 32:
						assert False
			return pack(self.__data_fmt, *lst)

		def to_dict(self):
			result = OrderedDict()
			for i, field in enumerate(self._fields_):
				name = field[0]
				result[name] = self.__data[i]
			return result

		def from_dict(self, data):
			assert len(self.__data) == 0
			for field in self._fields_:
				self.__data.append(data.get(field[0], 0))

		def from_bin(self, data):
			assert len(self.__data) == 0
			self.__data = unpack(self.__data_fmt, data)

		def tests(self):
			return
