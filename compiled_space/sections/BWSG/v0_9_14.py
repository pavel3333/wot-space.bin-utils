﻿""" BWSG (Static Geometry) """

from _base_json_section import *
from .v0_9_12 import PositionInfo, ModelInfo



class BWSG_Section_0_9_14(Base_JSON_Section):
	header = 'BWSG'
	int1 = 2

	@row_seek(True)
	def from_bin_stream(self, stream, row):
		self._data = Dict()
		entries = self.read_entries(stream, 12, '<3I')
		strings_size = unpack('<I', stream.read(4))[0]
		strings_start = stream.tell()
		_strings = Dict()
		for key, offset, length in entries:
			stream.seek(strings_start + offset)
			string = stream.read(length)
			assert getHash(string) == key
			_strings[key] = string.decode('ascii')
		stream.seek(strings_start + strings_size)
		self._data['strings'] = _strings
		self._data['models'] = self.read_entries(stream, ModelInfo)
		self._data['positions'] = self.read_entries(stream, PositionInfo)
		self._data['data_sizes'] = self.read_entries(stream, 4, '<I') # size BSGD section

	def to_bin(self):
		res = pack('<2I', 12, len(self._data['strings']))

		_list = []
		for _hash, _str in self._data['strings'].items():
			enc_str = _str.encode('ascii')
			_list.append((getHash(enc_str), enc_str))
		_list.sort()

		offset = 0
		for _hash, string in _list:
			res += pack('<3I', _hash, offset, len(string))
			offset += len(string) + 1
		res += pack('<I', offset)

		for _, string in _list:
			res += string + b'\x00'

		res += self.write_entries(self._data['models'], ModelInfo)
		res += self.write_entries(self._data['positions'], PositionInfo)
		res += self.write_entries(self._data['data_sizes'], 4, '<I')
		return res

	def add_str(self, _str):
		_hash = getHash(_str.encode('ascii'))
		self._data['strings'][_hash] = _str
		return _hash

	def from_vpath_list(self, bsgd, bsmo):
		import ResMgr
		i = len(self._data['models'])
		for vpath in bsmo.vertices_list:
			vertices_id = self.add_str(vpath)
			psec = ResMgr.openSection(vpath.replace('.primitives/', '.primitives_processed/'))
			bsec = psec.asBinary
			offset = 64
			vtype = bsec[:64].split(b'\0')[0].decode('ascii')
			if vtype.startswith('BPVT'):
				offset += 68
			vtype = vtype.replace('BPVT', '')
			vcount = unpack('<I', bsec[offset:offset+4])[0]
			offset += 4
			start_pos = len(bsgd._data)
			data_size = len(bsec) - offset
			add_to_16 = 16 - (data_size % 16)
			self._data['models'].append({
				'vertices_id': vertices_id,
				'id_from': i,
				'id_to': i+1,
				'vcount': vcount,
				'vtype_id': self.add_str(vtype),
			})
			self._data['positions'].append({
				'data_sizes_id': 0,
				'position': start_pos,
				'size': data_size,
				'type': [0, data_size//vcount], #[0, 32]
			})
			bsgd._data += bsec[offset:] + b'\0'*add_to_16
			bsmo._data['20_8'].append({
				'vertices_id': vertices_id,
				'data_size': data_size
			})
			bsmo._data['14_64'].append([
				1.0, 0.0, 0.0, 0.0,
				0.0, 1.0, 0.0, 0.0,
				0.0, 0.0, 1.0, 0.0,
				0.0, 0.0, 0.0, 1.0
			])
			i += 1
		self._data['data_sizes'] = [len(bsgd._data)]
