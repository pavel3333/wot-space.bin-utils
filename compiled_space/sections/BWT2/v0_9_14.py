""" BWT2 (Terrain 2) """

from _base_json_section import *
from .v0_9_12 import ChunkTerrain_v0_9_12



class TerrainSettings1_v0_9_14(CStructure):
	_size_ = 32

	_fields_ = [
		('chunk_size',     c_float     ), # space.settings/chunkSize or 100.0 by default
		('bounds',         c_int32 * 4 ), # space.settings/bounds
		('normal_map_fnv', c_uint32    ),
		('unknown_1_fnv',  c_uint32    ), # global_AM.dds, maybe tintTexture
		('unknown_2_fnv',  c_uint32    ), # maybe noiseTexture
		]

	_tests_ = {
		'chunk_size': { '==': 100.0 },
		# ...
		#'unknown_1_fnv': { '==': 2216829733 },
		'unknown_2_fnv': { '==': 2216829733 },
		}



class TerrainSettings2_v0_9_14(CStructure):
	_size_ = 156

	_fields_ = [
		('terrain_version',  c_uint32    ), # space.settings/terrain/version
		('other',            c_uint32*38 ),
		]

	_tests_ = {
		'terrain_version': { '==': 200 },
		}



class BWT2_Section_0_9_14(Base_JSON_Section):
	header = 'BWT2'
	int1 = 2

	_fields_ = [
		(dict, 'settings',      TerrainSettings1_v0_9_14 ),
		(list, 'cdatas',        ChunkTerrain_v0_9_12     ),
		(list, '3',             '<i'                     ),
		(dict, 'settings2',     TerrainSettings2_v0_9_14 ),
		(list, 'lod_distances', '<f'                     ), # space.settings/terrain/lodInfo/lodDistances
		(list, '6',             '<2i'                    ),
		(dict, '7',             '<I4fI'                  ),
		(list, '8',             '<6f'                    ),
		(list, '9',             '<3f'                    ),
		(list, '10',            '<4I'                    ),
		(list, '11',            '<i'                     ),
		(list, '12',            '<h'                     ),
		(list, '13',            '<I'                     ),
		(list, '14',            '<I'                     ),
		]
