""" BWT2 (Terrain 2) """

from _base_json_section import *



class TerrainSettings1_v0_9_12(CStructure):
	_size_ = 20

	_fields_ = [
		('chunk_size', c_float     ),
		('bounds',     c_int32 * 4 ),
		]

	_tests_ = {
		'chunk_size': { '==': 100.0 },
		# ...
		}



class ChunkTerrain_v0_9_12(CStructure):
	_size_ = 8

	_fields_ = [
		('resource_fnv', c_uint32 ),
		('loc_x',        c_int16  ),
		('loc_y',        c_int16  ),
		]



class BWT2_Section_0_9_12(Base_JSON_Section):
	header = 'BWT2'
	int1 = 1

	_fields_ = [
		(dict, 'settings',  TerrainSettings1_v0_9_12 ),
		(list, 'cdatas',    ChunkTerrain_v0_9_12     ),
		(list, '3',         '<i'                     ),
		(dict, 'settings2', '<I4fI'                  ),
		(list, '5',         '<6f'                    ),
		(list, '6',         '<3f'                    ),
		(list, '7',         '<4I'                    ),
		(list, '8',         '<i'                     ),
		(list, '9',         '<h'                     ),
		(list, '10',        '<I'                     ),
		(list, '11',        '<I'                     ),
		]
