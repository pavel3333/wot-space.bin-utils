""" BSMO (Static Model) """

from _base_json_section import *
from WSMO.utils import Section_WSMO_2, Section_WSMO_3
from .v0_9_12 import (
	VisualFileInfo_v0_9_12, MaterialInfo_v0_9_12,
	PrimitiveGroup_v0_9_12, AnimationInfo_v0_9_12,
	MinMax)



class VerticesDataSize_v0_9_20(CStructure):
	_size_ = 8

	_fields_ = [
		('vertices_fnv', c_uint32 ), # path to *.primitives/vertices
		('data_size',    c_uint32 ), # BWSG/positions/size
		]



class BSMO_Section_0_9_20(Base_JSON_Section):
	header = 'BSMO'
	int1 = 1

	_fields_ = [
		(list, 'lods_to_models',  '<2I'                     ),
		(list, '2_36',             VisualFileInfo_v0_9_12   ),
		(list, '3_8',              MaterialInfo_v0_9_12     ),
		(list, 'vboxes',           MinMax                   ), # *.model/visibilityBox
		(list, 'wsmo_1',           '<2I'                    ), # 0.9.12: WSMO['1']
		(list, 'wsmo_5',           '<I'                     ), # 0.9.12: WSMO['5']
		(list, 'model_extents',    '<f'                     ), # *.model/extent
		(list, 'model_to_pgroups', '<2I'                    ),
		(list, 'pgroups',          PrimitiveGroup_v0_9_12   ),
		(list, 'pgroups_nodes',    '<i'                     ), # link section 'pgroups' with 'visual_nodes'
		(list, '11_36',            AnimationInfo_v0_9_12    ),
		(list, '12_4',             '<i'                     ),
		(list, 'visual_nodes',     '<i16f'                  ), # *.visual/nodes
		(list, 'wsmo_4',           '<16f'                   ), # 0.9.12: WSMO['4']
		(list, 'wsmo_2',           Section_WSMO_2           ), # 0.9.12: WSMO['2']
		(list, 'wsmo_3',           Section_WSMO_3           ), # 0.9.12: WSMO['3']
		(list, '17_8',             VerticesDataSize_v0_9_20 ),
		]
