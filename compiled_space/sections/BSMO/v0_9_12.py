""" BSMO (Static Model) """

from _base_json_section import *



class VisualFileInfo_v0_9_12(CStructure):
	_size_ = 36

	_fields_ = [
		('bbox_min',       c_float * 3 ), # .visual/boundingBox/min
		('bbox_max',       c_float * 3 ), # .visual/boundingBox/max
		('primitives_fnv', c_uint32    ), # path to .primitives
		('from_',          c_int32     ),
		('to_',            c_int32     ),
		]



class MaterialInfo_v0_9_12(CStructure):
	_size_ = 8

	_fields_ = [
		('index',           c_uint32     ),
		('collision_flags', c_uint32, 8  ),
		('material_kind',   c_uint32, 8  ),
		('pad',             c_uint32, 16 ),
		]

	_tests_ = {
		'pad': { '==': 0 }
		}



class MinMax(CStructure):
	_size_ = 24

	_fields_ = [
		('min', c_float * 3 ),
		('max', c_float * 3 ),
		]



class PrimitiveGroup_v0_9_12(CStructure):
	_size_ = 28

	_fields_ = [
		('node_id_from',                c_int32      ), # -1 if renderSet/node = Scene Root
		('node_id_to',                  c_int32      ), # -1 if renderSet/node = Scene Root
		('index',                       c_uint32     ),
		('id_from_visual',              c_uint32     ), # *.visual/renderSet/geometry/primitiveGroup
		('vertices_fnv',                c_uint32     ), # *.primitives/vertices
		('indices_fnv',                 c_uint32     ), # *.primitives/indices
		('treat_as_world_space_object', c_uint32, 1  ), # *.visual/renderSet/treatAsWorldSpaceObject
		('pad',                         c_uint32, 31 ),
		]

	_tests_ = {
		'pad': { '==': 0 }
		}



class AnimationInfo_v0_9_12(CStructure):
	_size_ = 36

	_fields_ = [
		('animation_name_fnv', c_uint32 ), # .model/animation/name
		('frame_rate',         c_float  ), # .model/animation/frameRate
		('first_frame',        c_int32  ), # .model/animation/firstFrame
		('last_frame',         c_int32  ), # .model/animation/lastFrame
		('cognate_fnv',        c_uint32 ), # .model/animation/cognate
		('animation_path_fnv', c_uint32 ), # .model/animation/nodes + .animation
		('from_',              c_uint32 ),
		('to_',                c_uint32 ),
		('anca_path_fnv',      c_uint32 ), # .model/nodefullVisual + .anca
		]



class BSMO_Section_0_9_12(Base_JSON_Section):
	header = 'BSMO'
	int1 = 1

	_fields_ = [
		(list, 'lods_to_models',  '<2I'                   ),
		(list, '2_36',             VisualFileInfo_v0_9_12 ),
		(list, '3_8',              MaterialInfo_v0_9_12   ),
		(list, 'vboxes',           MinMax                 ), # *.model/visibilityBox
		(list, 'model_extents',    '<f'                   ), # *.model/extent
		(list, 'model_to_pgroups', '<2I'                  ),
		(list, 'pgroups',          PrimitiveGroup_v0_9_12 ),
		(list, 'pgroups_nodes',    '<i'                   ), # link section 'pgroups' with 'visual_nodes'
		(list, 'animations',       AnimationInfo_v0_9_12  ),
		(list, '10_4',             '<i'                   ),
		(list, 'visual_nodes',     '<i16f'                ), # *.visual/nodes
		]
