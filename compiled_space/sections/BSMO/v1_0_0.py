""" BSMO (Static Model) """

from _base_json_section import *
from .v0_9_20 import VerticesDataSize_v0_9_20
from .v0_9_12 import (
	VisualFileInfo_v0_9_12, MaterialInfo_v0_9_12,
	PrimitiveGroup_v0_9_12, AnimationInfo_v0_9_12,
	MinMax)



class Section_WSMO_2_v1_0_0(CStructure):
	'''
	destructibles.xml/fallingAtoms
	in WoT: 55 8B EC 8B 41 50 53 56 57 8B 48 20 8B B0 BC 00 00 00 8B 45 08
	'''
	_size_ = 48

	_fields_ = [
		('lifetime_effect_fnv',  c_uint32    ), # lifetimeEffect
		('fracture_effect_fnv',  c_uint32    ), # fractureEffect
		('touchdown_effect_fnv', c_uint32    ), # touchdownEffect
		('unknown',              c_float     ),
		('effect_scale',         c_float     ), # effectScale
		('physic_params',        c_float * 7 ), # physicParams
		]

	_tests_ = {
		'unknown': { '==': 1.0 }
		}



class Section_WSMO_3_v1_0_0(CStructure):
	'''
	destructibles.xml/fragiles
	destructibles.xml/structures
	'''
	_size_ = 36

	_fields_ = [
		('lifetime_effect_fnv',   c_uint32 ), # lifetimeEffect
		('effect_fnv',            c_uint32 ), # effect / ramEffect
		('decay_effect_fnv',      c_uint32 ), # decayEffect
		('hit_effect_fnv',        c_uint32 ), # hitEffect
		('_4',                    c_float  ),
		('effect_scale',          c_float  ), # effectScale
		('id_1',                  c_int32  ),
		('id_2',                  c_int32  ),
		('entry_type',            c_uint32 ), # fragiles:0,2, structures:1
		]

	_tests_ = {
		'_4': { '==': 1.0 },
		'id_1': { '>=': -1 },
		'id_2': { '>=': -1 },
		'entry_type': { 'in': (0, 1, 2, 3) },
		}



class VisualNode_v1_0_0(CStructure):
	_size_ = 72

	_fields_ = [
		('index',          c_int32      ),
		('transform',      c_float * 16 ),
		('identifier_fnv', c_uint32     ),
		]



class HavokInfo_v1_0_0(CStructure):
	_size_ = 8

	_fields_ = [
		('havok_fnv',    c_uint32 ),
		('mat_name_fnv', c_uint32 ),
		]



class BSMO_Section_1_0_0(Base_JSON_Section):
	header = 'BSMO'
	int1 = 2

	_fields_ = [
		(list, 'lods_to_models',  '<2I'                     ),
		(list, '2_36',             VisualFileInfo_v0_9_12   ),
		(list, '3_8',              MaterialInfo_v0_9_12     ),
		(list, 'vboxes',           MinMax                   ),
		(list, 'wsmo_1',           '<2I'                    ),
		(list, 'wsmo_5',           '<I'                     ),
		(list, 'model_extents',    '<f'                     ),
		(list, 'model_to_pgroups', '<2I'                    ),
		(list, 'pgroups',          PrimitiveGroup_v0_9_12   ),
		(list, 'pgroups_nodes',    '<i'                     ),
		(list, 'model_animations', AnimationInfo_v0_9_12    ),
		(list, '12_4',             '<i'                     ),
		(list, '13_8',             '<2I'                    ),
		(list, '14_4',             '<I'                     ),
		(list, 'visual_nodes',     VisualNode_v1_0_0        ),
		(list, 'wsmo_4',           '<16f'                   ),
		(list, 'wsmo_2',           Section_WSMO_2_v1_0_0    ),
		(list, 'wsmo_3',           Section_WSMO_3_v1_0_0    ),
		(list, 'havok_info',       HavokInfo_v1_0_0         ),
		(list, '20_8',             VerticesDataSize_v0_9_20 ),
		]

	def init(self):
		self.last_model_index = 0
		self.last_material_index = 0
		self.last_pgroup_index = 0
		self.resources = {}
		self.vertices_list = []

	def save_model(self, m_res, bwst, bwal, bsma):
		if m_res in self.resources:
			return self.resources[m_res]

		import ResMgr
		from anca_reader import anca_load, animation_load

		model_sec = ResMgr.openSection(m_res)
		if model_sec is None:
			return None

		visual_path = model_sec.readString('nodefullVisual', model_sec.readString('nodelessVisual'))
		visual = ResMgr.openSection(visual_path + '.visual_processed')
		if visual is None:
			visual = ResMgr.openSection(visual_path + '.visual')
			if visual is None:
				return None

		self.resources[m_res] = {
			'id': self.last_model_index
		}

		bwal.add(6, bwst.add_str(m_res))

		self._data['lods_to_models'].append([self.last_model_index, self.last_model_index])

		self._data['13_8'].append([4294967295, 4294967295])
		self._data['wsmo_1'].append([0, 4294967295])
		self._data['wsmo_5'].append(0)
		self._data['havok_info'].append({
			'havok_fnv': 0,
			'mat_name_fnv': 0
		})

		if model_sec.has_key('extent'):
			m_extent = model_sec.readFloat('extent')**2
		else:
			MAX_FLOAT = 3.4028234663852886e+38 # const
			m_extent = MAX_FLOAT
		self._data['model_extents'].append(m_extent)

		node_start = len(self._data['visual_nodes'])

		nodes_data = Dict()
		if visual['node'].has_key('node'):
			def pack_elem(elem, node_id, identifier):
				vec16 = get_vec16_from_mat4x4(elem.readMatrix('transform'))
				nodes_data[identifier] = (len(self._data['visual_nodes']), len(self._data['visual_nodes'])-node_start)
				self._data['visual_nodes'].append({
					'index': node_id,
					'transform': vec16,
					'identifier_fnv': bwst.add_str(identifier)
				})
			node_root = visual['node']
			pack_elem(node_root, -1, 'Scene Root')
			def rec_node(node):
				node_id = 0
				for elem_name, elem in node.items():
					if elem_name != 'node':
						continue
					node_id += 1
					identifier = elem.readString('identifier')
					pack_elem(elem, node_id, identifier)
					node_id += rec_node(elem)
				return node_id
			rec_node(node_root)
		self.resources[m_res]['nodes'] = nodes_data

		primitives = visual.readString('primitivesName', visual_path) + '.primitives'

		new_pgroup_index = self.last_pgroup_index
		new_material_index = self.last_material_index

		for curName, curSect in visual.items():
			if curName != 'renderSet':
				continue

			treatAsWorldSpaceObject = curSect.readBool('treatAsWorldSpaceObject', False)

			primitives_v = primitives + '/' + curSect.readString('geometry/vertices')
			if primitives_v not in self.vertices_list:
				self.vertices_list.append(primitives_v)
			primitives_i = primitives + '/' + curSect.readString('geometry/primitive')

			rs_nodes = curSect.readStrings('node')
			if len(rs_nodes) == 1 and rs_nodes[0] == 'Scene Root': # and treatAsWorldSpaceObject ?
				node_id_from = -1
				node_id_to = -1
			else:
				node_id_from = len(self._data['pgroups_nodes'])
				for _node in rs_nodes:
					self._data['pgroups_nodes'].append(nodes_data[_node][1])
				node_id_to = len(self._data['pgroups_nodes']) - 1

			for curSubName, curSubSect in curSect['geometry'].items():
				if curSubName != 'primitiveGroup':
					continue

				bsma.from_visual_material(bwst, curSubSect['material'])
				collisionFlags = curSubSect.readInt('collisionFlags', 0)
				materialKind = curSubSect.readInt('materialKind', 0)

				self._data['3_8'].append({
					'index': curSubSect.asInt,
					'collision_flags': collisionFlags,
					'material_kind': materialKind
				})

				new_material_index += 1

				self._data['pgroups'].append({
					'node_id_from': node_id_from,
					'node_id_to': node_id_to,
					'index': new_pgroup_index, # maybe index BSMO/5_8
					'id_from_visual': curSubSect.asInt,
					'vertices_fnv': bwst.add_str(primitives_v),
					'indices_fnv': bwst.add_str(primitives_i),
					'treat_as_world_space_object': treatAsWorldSpaceObject
					})
				new_pgroup_index += 1

		self._data['model_to_pgroups'].append([self.last_pgroup_index, new_pgroup_index-1])
		self.last_pgroup_index = new_pgroup_index

		bbmin = visual.readVector3('boundingBox/min')
		bbmax = visual.readVector3('boundingBox/max')
		self._data['2_36'].append({
			'bbox_min': bbmin.tuple(),
			'bbox_max': bbmax.tuple(),
			'primitives_fnv': bwst.add_str(primitives),
			'from_': self.last_material_index,
			'to_': new_material_index-1
		})
		self.last_material_index = new_material_index

		vboxmin = model_sec.readVector3('visibilityBox/min', bbmin)
		vboxmax = model_sec.readVector3('visibilityBox/max', bbmax)
		self._data['vboxes'].append({
			'min': vboxmin.tuple(),
			'max': vboxmax.tuple()
		})

		self.resources[m_res]['animations'] = {}
		anim_id = 0
		for name, sect in model_sec.items():
			if name != 'animation':
				continue
			anim_name = sect.readString('name')
			animation_path = sect.readString('nodes') + '.animation'
			anim_frameRate = sect.readFloat('frameRate')
			anca_path = visual_path + '.anca'

			anca_data = ResMgr.openSection(anca_path)
			if anca_data is None:
				animation_data = ResMgr.openSection(animation_path)
				if animation_data is None:
					continue
				unpackedAnimationData = animation_load(animation_data.asBinary)
			else:
				unpackedAnimationData = anca_load(anca_data.asBinary, animation_path)

			from_id = len(self._data['12_4'])

			for achannel_identifier in unpackedAnimationData['animation_channels']:
				if achannel_identifier in nodes_data:
					node_id = nodes_data[achannel_identifier][1]
				else:
					node_id = -1
				self._data['12_4'].append(node_id)

			to_id = len(self._data['12_4']) - 1

			self._data['model_animations'].append({
				'animation_name_fnv': bwst.add_str(anim_name),
				'frame_rate': anim_frameRate,
				'first_frame': 0,
				'last_frame': -1,
				'cognate_id': 0,
				'animation_path_fnv': bwst.add_str(animation_path),
				'from_': from_id,
				'to_': to_id,
				'anca_path_fnv': bwst.add_str(anca_path)
			})
			self.resources[m_res]['animations'][anim_name] = len(self._data['model_animations']) - 1
			anim_id += 1

		self.last_model_index += 1

		return self.resources[m_res]
