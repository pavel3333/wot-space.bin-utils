""" BWPs (Particles) """

from BWPs.utils import *



__all__ = ('BWPs_Section_0_9_12',)



class BWPs_Section_0_9_12(Base_JSON_Section):
	header = 'BWPs'
	int1 = 1

	_fields_ = [
		(list, 'particles', ParticleInfo),
		]

	def from_chunk(self, bwst, bwal, particles):
		import ResMgr
		for value in particles:
			res = value.readString('resource')
			res_sec = ResMgr.openSection(res)
			seedTime = 0.1

			resource_fnv = bwst.add_str(res)

			bwal.add(1, resource_fnv)

			if res_sec is not None:
				seedTime = res_sec.readFloat('seedTime', 0.1)
			self._data['particles'].append({
				'transform': get_vec16_from_mat4x4(value.readMatrix('transform')),
				'resource_fnv': resource_fnv,
				'reflection_visible': value.readBool('reflectionVisible'),
				'seed_time': seedTime
			})

	def to_xml(self, chunks):
		write = lambda *args: self._add2xml(el, *args)

		for it in self._data['particles']:
			chunk, transform = chunks.get_by_transform(it['transform'])
			el = ET.SubElement(chunk, 'particles')

			#write('visibilityMask', 4294967295)
			write('resource',  chunks.gets(it['resource_fnv']) )
			write('transform', transform                       )



"""
.chunk example:

<root>
	<particles>
		<visibilityMask>	4294967295	</visibilityMask>
		<resource>	particles/Environment/interior/01_karelia_butterfly_white.xml	</resource>
		<transform>
			<row0>	0.999999 0.000000 0.000000	</row0>
			<row1>	0.000000 0.999999 0.000000	</row1>
			<row2>	0.000000 0.000000 0.999999	</row2>
			<row3>	80.700310 21.845163 76.836479	</row3>
		</transform>
		<reflectionVisible>	false	</reflectionVisible>
	</particles>
</root>
"""
