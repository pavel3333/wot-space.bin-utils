﻿""" BSMI (Model Instances) """

from _base_json_section import *



class ChunkModel(CStructure):
	'''
	data from <model> section of .chunk file
	'''
	_size_ = 8

	_fields_ = [
		('casts_shadow',                 c_uint32, 1  ), # model/castsShadow
		('reflection_visible',           c_uint32, 1  ), # model/reflectionVisible
		('pad1',                         c_uint32, 1  ),
		('shadow_proxy',                 c_uint32, 1  ), # model/shadowProxy
		('casts_local_shadow',           c_uint32, 1  ), # model/castsLocalShadow
		('not_ignores_objects_farplane', c_uint32, 1  ), # negative model/ignoresObjectsFarplane
		('has_animations',               c_uint32, 1  ), # if .model/animation sections
		('pad2',                         c_uint32, 25 ),
		('unknown',                      c_uint32     ),
		]

	_tests_ = {
		# ...
		#'pad1': { '==': 0 },
		# ...
		#'pad2': { '==': 0 },
		'unknown': { 'in': (0, 1) },
		}



class ModelAnimation(CStructure):
	'''
	data from .model file
	'''
	_size_ = 16

	_fields_ = [
		('model_index',           c_uint32 ),
		('animation_id',          c_int32  ), # index .model/animation from .chunk/model/animation/name
		('unknown',               c_uint32 ),
		('frame_rate_multiplier', c_float  ), # .chunk/model/animation/frameRateMultiplier
		]

	_tests_ = {
		# ...
		'animation_id': { '>=': -1 },
		'unknown': { '==': 0 },
		'frame_rate_multiplier': { '>=': 0.0 },
		}



class BSMI_Section_0_9_12(Base_JSON_Section):
	header = 'BSMI'
	int1 = 1

	_fields_ = [
		(list, 'transforms',       '<16f'         ),
		(list, 'chunk_models',     ChunkModel     ),
		(list, 'bsmo_models_id',   '<I'           ),
		(list, 'animations_id',    '<i'           ),
		(list, 'model_animation',  ModelAnimation ),
		(list, '6_4',              '<I'           ),
		]
