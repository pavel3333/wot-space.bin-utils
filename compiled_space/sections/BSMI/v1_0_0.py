﻿""" BSMI (Model Instances) """

from _base_json_section import *
from .v0_9_12 import ModelAnimation



class ChunkModel_v1_0_0(CStructure):
	'''
	data from <model> section of .chunk file
	'''
	_size_ = 8

	_fields_ = [
		('casts_shadow',                 c_uint32, 1  ), # model/castsShadow
		('pad1',                         c_uint32, 1  ),
		('casts_local_shadow',           c_uint32, 1  ), # model/castsLocalShadow
		('not_ignores_objects_farplane', c_uint32, 1  ), # negative model/ignoresObjectsFarplane
		('always_dynamic',               c_uint32, 1  ), # model/alwaysDynamic
		('unknown1',                     c_uint32, 1  ),
		('has_animations',               c_uint32, 1  ), # if .model/animation sections
		('pad2',                         c_uint32, 25 ),
		('unknown',                      c_uint32     ),
		]

	_tests_ = {
		# ...
		#'pad1': { '==': 0 },
		# ...
		#'pad2': { '==': 0 },
		'unknown': { 'in': (0, 1) },
		}



class SequencesInfo_v1_0_0(CStructure):
	_size_ = 28

	_fields_ = [
		('i1',           c_uint32 ),
		('resource_fnv', c_uint32 ),
		('loop_count',   c_float  ), # loopCount
		('i4',           c_uint32 ),
		('speed',        c_float  ), # speed
		('delay',        c_float  ), # delay
		('i7',           c_uint32 ),
		]



class BSMI_Section_1_0_0(Base_JSON_Section):
	header = 'BSMI'
	int1 = 3

	_fields_ = [
		(list, 'transforms',       '<16f'               ),
		(list, 'chunk_models',     ChunkModel_v1_0_0    ),
		(list, 'visibility_masks', '<I'                 ),
		(list, 'bsmo_models_id',   '<I'                 ),
		(list, 'animations_id',    '<i'                 ),
		(list, 'model_animation',  ModelAnimation       ),
		(list, 'sequences',        SequencesInfo_v1_0_0 ),
		(list, '8_4',              '<I'                 ),
		(list, '9_12',             '<3I'                ), # 0.9.12: WSMI['1_12']
		(list, '10_4',             '<I'                 ),
		(list, '11_20',            '<5f'                ),
		]

	def save_models(self, bwst, bwal, bsmo, bsma, bwsg, bsgd, models):
		import ResMgr
		bsmo.init()
		bsma.init()
		model_index = 0
		for value in models:
			m_res = value.readString('resource')
			model = bsmo.save_model(m_res, bwst, bwal, bsma)
			if model is None:
				continue
			mat4x4 = value.readMatrix('transform')
			m_transform = get_vec16_from_mat4x4(mat4x4)
			self._data['transforms'].append(m_transform)
			m_castShadows = value.readBool('castsShadow', True)
			m_castsLocalShadow = value.readBool('castsLocalShadow', False)
			m_alwaysDynamic = value.readBool('alwaysDynamic', False)

			# TODO:
			ignores_objects_farplane = not value.readBool('ignoresObjectsFarplane', False)

			self._data['chunk_models'].append({
				'casts_shadow': m_castShadows,
				'casts_local_shadow': m_castsLocalShadow,
				'always_dynamic': m_alwaysDynamic,
				'not_ignores_objects_farplane': ignores_objects_farplane,
				'has_animations': bool(model['animations']),
				'unknown': 1
			})
			m_visibilityMask = value.readInt64('visibilityMask', 4294967295)
			self._data['visibility_masks'].append(m_visibilityMask)
			self._data['bsmo_models_id'].append(model['id'])
			self._data['9_12'].append([4294967295, 4294967295, 4294967295])
			self._data['11_20'].append([-1.0, -1.0, -1.0, -1.0, -1.0])
			anim_name = value.readString('animation/name')
			if anim_name in model['animations']:
				self._data['animations_id'].append(len(self._data['8_4']))
				animation_id = model['animations'][anim_name]
				frame_rate_multiplier = value.readFloat('animation/frameRateMultiplier', 1.0)
				for i, _ in model['nodes'].values():
					self._data['8_4'].append(i)
				self._data['model_animation'].append({
					'model_index': model_index,
					'animation_id': animation_id,
					'unknown': 0,
					'frame_rate_multiplier': frame_rate_multiplier
				})
			else:
				self._data['animations_id'].append(-1)
			model_index += 1

		# bwsg.from_vpath_list(bsgd, bsmo)

	def to_xml(self, chunks):
		bsmo = chunks.secs['BSMO']

		for i, id in enumerate(self._data['bsmo_models_id']):
			self.__model_to_xml(i, id, bsmo, chunks)

	def __model_to_xml(self, i, model_id, bsmo, chunks):
		# very bad solution
		path = chunks.gets(bsmo._data['2_36'][model_id]['primitives_fnv'])

		if not path:
			return

		path = path.replace('.primitives', '.model')

		chunk, transform = chunks.get_by_transform(self._data['transforms'][i])

		el = ET.SubElement(chunk, 'model')
		write = lambda *args: self._add2xml(el, *args)

		mdl = self._data['chunk_models'][i]

		write('transform',              transform                                     )
		write('visibilityMask',         self._data['visibility_masks'][i]             )
		write('resource',               path                                          )
		write('castsShadow',            bool(mdl['casts_shadow'])                     )
		write('alwaysDynamic',          bool(mdl['always_dynamic'])                   )
		write('ignoresObjectsFarplane', not bool(mdl['not_ignores_objects_farplane']) )
		# TODO: more
