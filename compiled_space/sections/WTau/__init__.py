""" WTau (Audio) """

from _base_json_section import *



__all__ = ('WTau_Section_0_9_12',)



class Audio1(CStructure):
	_size_ = 24

	_fields_ = [
		('wwevent_name_fnv', c_uint32    ),
		('event_name_fnv',   c_uint32    ),
		('max_distance',     c_float     ),
		('position',         c_float * 3 ),
		]



class WTau_Section_0_9_12(Base_JSON_Section):
	header = 'WTau'
	int1 = 2

	_fields_ = [
		(list, '1', Audio1 ),
		(list, '2', '<6I'  ),
		]

	def from_chunk(self, bwst, list_audio):
		for value in list_audio:
			wwevent_name = value.readString('wweventName', '')
			event_name = value.readString('eventName', '')
			if value.has_key('transform'):
				position = value.readMatrix('transform').translate
			else:
				position = value.readVector3('position')
			self._data['1'].append({
				'wwevent_name_fnv': bwst.add_str(wwevent_name),
				'event_name_fnv': bwst.add_str(event_name),
				'max_distance': value.readFloat('maxDistance'),
				'position': position.tuple()
			})



"""
.chunk example:

<root>
	<audio>
		<visibilityMask>4294967295</visibilityMask>
		<transform>
			<row0>1.000000 0.000000 0.000000</row0>
			<row1>0.000000 1.000000 0.000000</row1>
			<row2>0.000000 0.000000 1.000000</row2>
			<row3>76.994934 1.227502 78.178879</row3>
		</transform>
		<wweventName>wweventName</wweventName>
		<eventName>eventName</eventName>
		<maxDistance>60.000000</maxDistance>
	</audio>
</root>
"""
